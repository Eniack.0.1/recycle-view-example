package uselinux.me.recyclerview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ListAdaptor adaptor ;
    private RecyclerView list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        list =findViewById(R.id.list);

        list.setLayoutManager(new GridLayoutManager(this,3));

        ArrayList<String> datalist =new ArrayList<>();

        // dummy data
        for (int i=0 ; i<25;i++){
            datalist.add("data"+(i+1));
        }

        adaptor =new ListAdaptor(datalist);

        list.setAdapter(adaptor);

    }
}
