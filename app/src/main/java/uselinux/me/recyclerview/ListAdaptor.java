package uselinux.me.recyclerview;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.ArrayList;

public class ListAdaptor extends RecyclerView.Adapter<ListItemViewHolder> {

    private ArrayList<String> array;

    public ListAdaptor(ArrayList<String> array) {
        this.array = array;
    }

    @Override
    public ListItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ListItem listItem =new ListItem(parent.getContext());
        return new ListItemViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull ListItemViewHolder holder, int position) {
        holder.setData(array.get(position),position);
    }

    @Override
    public int getItemCount() {
        return array.size();
    }
}
