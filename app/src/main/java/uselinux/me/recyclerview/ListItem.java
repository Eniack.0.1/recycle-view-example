package uselinux.me.recyclerview;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


public class ListItem extends RelativeLayout {

    private View root;
    private TextView text;
    private String data;

    public ListItem(Context context) {
        super(context);
        initialize(context);
    }


    private void initialize(Context context) {
        inflate(context, R.layout.list_item, this);
        text = findViewById(R.id.text);
        root = findViewById(R.id.root);
    }

    public String getData() {
        return data;
    }

    public void setData(final String data, final int position) {
        this.data = data;
        this.text.setText(data);
        setOnClickListener(null);
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "data= " + data + "\nposition = " + position, Toast.LENGTH_SHORT).show();
            }
        });

    }
}
