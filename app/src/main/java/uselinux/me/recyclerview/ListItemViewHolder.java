package uselinux.me.recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.View;

public class ListItemViewHolder extends RecyclerView.ViewHolder {

    public ListItemViewHolder(View itemView) {
        super(itemView);
    }

    public void setData(String data,int position){
        ((ListItem)itemView).setData(data,position);
    }
}
